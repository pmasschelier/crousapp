# Installation

```bash
git clone git@gitlab.com:pmasschelier/crousapp.git
cd crousapp
npm install
npm run dev
```

Create a .env file in the project directory containing :
```sh
BASEURL="http://localhost:3000"
PORT=3000
NODE_ENV="DEV"
COOKIE_SECRET="rkjxhrz,lr,zhrkehethemlrlri(_u(çè895748954U5IHJKRNJGDKJB"
```
*Note : COOKIE_SECRET can be anything.*

# Creation

```bash
npx express-generator --ejs  --git CrousApp
cd CrousApp
npm i bcrypt dotenv
```

## Database

```sql
CREATE TABLE IF NOT EXISTS Owners (
	id INTEGER PRIMARY KEY,
	username TEXT,
	hashpwd TEXT,
	business INTEGER,
	FOREIGN KEY (business) REFERENCES Businesses(id)
);

CREATE TABLE IF NOT EXISTS Businesses (
	id INTEGER PRIMARY KEY,
	name TEXT,
	description TEXT,
	img TEXT,
	location TEXT,
	hour_open TEXT,
	hour_close TEXT
);
```

Corresponding types can be found in back/types.ts

```ts
export interface LoginInfos {
	username: string,
	hashpwd: string
}

export interface Owner extends LoginInfos {
	id: number
	business: number
}

export interface Business {
	id: number
	name: string
	description: string
	img: string
	location: string
	hour_open: string
	hour_close: string
}
```