import createError from 'http-errors';
import express from 'express';
import path from 'path';
import logger from 'morgan'; // Logging middleware

import debugLogger from 'debug';
import { CONFIG } from './back/config';
import session from 'express-session';
const debug = debugLogger('crousapp:server');

const indexRouter = require('./routes/index');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/* Concise output colored by response status for development use.
 * The :status token will be colored green for success codes,
 * red for server error codes, yellow for client error codes,
 * cyan for redirection codes, and uncolored for information codes. */
app.use(logger('dev'));

app.use(express.urlencoded({ extended: false }));

// Allow to use cookie session
app.use(session({
    secret: process.env.COOKIE_SECRET as string,
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: !CONFIG.DEV
    }
}))

/* We serve the public directory as a static files server */
app.use(express.static(path.join(__dirname, 'public')));

// Passer l'authentification à ejs
app.use(function (req: any, res: any, next) {
	res.locals.user = req.session.user;
	debug(res.locals);
	next();
});

app.use(indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err: any, req: any, res: any, next: () => void) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = CONFIG.DEV ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

app.listen(CONFIG.PORT, () => {
	debug(`Listening on ${process.env.BASEURL}`);
});
