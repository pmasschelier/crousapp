const dotenv = require('dotenv'); // Manage .env file

dotenv.config();

export const CONFIG = {
	PORT: process.env.PORT || 3001,
	DEV: process.env.NODE_ENV === 'DEV'
};