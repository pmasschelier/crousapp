import sqlite from 'sqlite3';
import { Business, LoginInfos, Owner } from './types';

import debugLogger from 'debug';
const debug = debugLogger('crousapp:server');

const db = new sqlite.Database('crous.db');

class asyncSql {
	/*
	Transforme les requêtes SQL qui répondent avec des callback en async/await de Javascript.
	Permet du code beaucoup plus propre.
	 */
	static all(req: string, params?: any[]): Promise<any[]> {
		return new Promise((resolve, reject) => {
			db.all(req, params, function (err: (Error | null), rows: any[]) {
				if (err) reject(err);
				else resolve(rows);
			});
		});
	}

	static get(req: string, params?: any[]): Promise<any> {
		return new Promise((resolve, reject) => {
			db.get(req, params, function (err: (Error | null), row: any) {
				if (err) reject(err);
				else resolve(row);
			});
		});
	}

	static run(req: string, params?: any[]): Promise<void> {
		return new Promise((resolve, reject) => {
			db.run(req, params, function (err: (Error | null), res: any) {
				if (err) reject(err);
				else resolve();
			});
		});
	}
}

export namespace DB {

abstract class Table<Row extends { id: number }> {
	abstract get TABLE(): string;

	getAll(): Promise<Row[]> {
		return asyncSql.get(`SELECT * FROM ${this.TABLE}`)
	}

	getById(id: number): Promise<Row> {
		return asyncSql.get(`SELECT * FROM ${this.TABLE} WHERE id = ?`, [id])
	}
}

class OwnersTable extends Table<Owner> {
	get TABLE() { return 'Owners'; }

	create(infos: LoginInfos) {
		return asyncSql.run(`INSERT INTO ${this.TABLE}(username, hashpwd) VALUES (?, ?)`, [infos.username, infos.hashpwd]);
	}

	getByLoginInfos(infos: LoginInfos): Promise<Owner | undefined> {
		return asyncSql.get(`SELECT * FROM ${this.TABLE} WHERE username = ? AND hashpwd = ?`, [...[infos]]);
	}

	getByUsername(username: string): Promise<Owner | undefined> {
		return asyncSql.get(`SELECT * FROM ${this.TABLE} WHERE username = ?`, [username]);
	}
}

export const Owners = new OwnersTable;

class BusinessesTable extends Table<Business> {
	get TABLE() { return 'Businesses'; }
}

export const Businesses = new BusinessesTable;

}