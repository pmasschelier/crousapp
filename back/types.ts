export interface LoginInfos {
	username: string,
	hashpwd: string
}

export interface Owner extends LoginInfos {
	id: number
	business: number
}

export interface Business {
	id: number
	name: string
	description: string
	img: string
	location: string
	hour_open: string
	hour_close: string
}