import { DB } from "../back/db";

const express = require('express');
const router = express.Router();
const bcrypt = require("bcrypt")
const saltRounds = 10

import debugLogger from 'debug';
const debug = debugLogger('crousapp:server');

const params = {title: 'Crous App'};

/* GET home page. */
router.get('/', (req: any, res: any) => {
	res.render('index', params);
});

router.get('/login', (req: any, res: any) => {
	res.render('login', params);
});

router.post('/login', async (req: any, res: any) => {
	const hash = await bcrypt.hash(req.body.password, saltRounds);
	const user = await DB.Owners.getByLoginInfos({username: req.body.username, hashpwd: hash});
	
	if(user) {
		req.session.user = user;
		res.redirect('/find');
	}
	res.status(400).send(`Invalid username or password`);
});

router.get('/signup', (req: any, res: any) => {
	res.render('signup', params);
});

router.post('/signup', async (req: any, res: any) => {
	let user = await DB.Owners.getByUsername(req.body.username);
	
	if(user) {
		res.status(304); // HTTP Code 'Not Modified';
		res.send('This username is already used');
		return;
	}
	
	const hash = await bcrypt.hash(req.body.password, saltRounds);
	await DB.Owners.create({username: req.body.username, hashpwd: hash});

	user = await DB.Owners.getByUsername(req.body.username);
	req.session.user = user;

	res.redirect('/find');
});

router.get('/find', async (req: any, res: any) => {
	const businesses = DB.Businesses.getAll();
	//debug(`USER: `, req.locals.user);
	res.render('list', { ...params, businesses });
});

module.exports = router;
